# -*- coding: utf-8 -*- Alumne: Joan Bruguera Micó / Algorísmica Avançada.

# Representem el Sudoku com un graf de 81 nodes, un per cada cel·la:
# 00 01 02 | 03 04 05 | 06 07 08
# 09 10 11 | 12 13 14 | 15 16 17
# 18 19 20 | 21 22 23 | 24 25 26
# -------- + -------- + --------
# 27 28 29 | 30 31 32 | 33 34 35
# 36 37 38 | 39 40 41 | 42 43 44
# 45 46 47 | 48 49 50 | 51 52 53
# -------- + -------- + --------
# 54 55 56 | 57 58 59 | 60 61 62
# 63 64 65 | 66 67 68 | 69 70 71
# 72 73 74 | 75 76 77 | 78 79 80
#
# L'estat intern del Sudoku s'emmagatzema en dues llistes de 81 elements:
# - numero: Indica el número associat a la cel·la actual, o 0 si no n'hi ha cap.
# - disponibles: Nombre de 9 bits que indica quins nombres potencialment poden anar a una cel·la.
#                El bit més baix indica que el nombre 1 pot anar a la cel·la, etc.
#
#                El motiu d'usar un nombre de 9 bits (en comptes de, per exemple, una llista)
#                es que, apart d'utilitzar molt poca memòria,
#                met realitzar operacions de conjunts de forma molt ràpida,
#                per exemple, cardinalitat -> nombre de bits, unió -> OR, intersecció -> AND...
#
#                Per exemple, si en una cel·la poden anar els numeros 1 i 5, el valor
#                associat en la matriu disponibles seria 17 = 0b000010001 = 2**4 + 2**0
class Sudoku:
	# Distingim 27 regions en un Sudoku: 9 files, 9 columnes, 9 quadrats
	# Començem creant llistes que continguin cada una de les regions,
	# per tal de poder crear les llistes de connexions entre cel·les,
	# i aplicar l'algorisme de poda per regions posteriorment
	files = [[f*9+c for c in range(9)] for f in range(9)]
	columnes = [[f*9+c for f in range(9)] for c in range(9)]
	quadrats = [[(Mq/3)*27+(Mq%3)*3+(mq/3)*9+(mq%3) for mq in range(9)] for Mq in range(9)]

	regions = files + columnes + quadrats

	# Per cada cel·la, creem una llista que ens indiqui amb quines cel·les
	# comparteix alguna regió. Per exemple, el node 11 esta enllaçat amb:
	# [0, 1, 2, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 29, 38, 47, 56, 65, 74]
	# Utilitzarem aquesta llista per actualitzar la llista 'disponibles' eficientment.
	connexions = []
	for n in range(81):
		s = set(files[n/9]) | set(columnes[n%9]) | set(quadrats[(n/27)*3+(n/3)%3])
		s.remove(n)
		connexions.append(list(s))

	# Taula que indica el nombre de bits posats a 1 d'un valor de fins a 9 bits
	nbits9 = [bin(x).count("1") for x in range(2**9)]

	# Taula que indica el bit més alt d'un valor de fins a 9 bits
	altbits9 = [len(bin(x))-2 for x in range(2**9)]

	# Crea un Sudoku buit
	def __init__(self):
		self.numero = [0 for i in range(81)] # Tots els nombres estan indeterminats
		self.disponibles = [0b111111111 for i in range(81)] # Qualsevol nombre esta disponible
		self.nProves = 0

	# Assigna un número a una cel·la del Sudoku
	# Retorna False si l'assignació genera un Sudoku invàlid
	def assigna_numero(self, posicio, numero):
		# Mirem si és possible assignar aquest número
		if self.disponibles[posicio] & 2**(numero-1) == 0:
			return False

		# Assignem el número a la casella actual
		self.numero[posicio] = numero
		self.disponibles[posicio] = 0

		# Esborrem aquest número de les opcions disponibles dels nodes veïns
		for vei in Sudoku.connexions[posicio]:
			self.disponibles[vei] &= ~(2**(numero-1))

		return True

	# Rutina principal de resolució de Sudokus, amb backtracking, ramificació i poda.
	# L'esquema bàsic de l'algorisme és el següent:
	# - En primer lloc, busquem cel·les en les que només hi ha una opció possible i les omplim.
	#   Aquest pas de ramificació és barat i ens permet solucionar ràpidament situacions senzilles.
	#
	# - Si no hi ha cap cel·la amb una opció possible, fem un pas de poda utilitzant el procés
	#   explicat en la funció busca_eliminacions(). Si es poda, aleshores es torna al primer pas.
	#   Aquest pas és car, però potencialment evita haver de fer un backtracking encara més costós.
	#
	# - Si no hi ha cap cel·la buida ni hi ha cap forma de podar l'arbre, aleshores fem una
	#   prova i error amb backtracking. Com a optimització en la ramificació, escollim la cel·la
	#   amb menys opcions, per tal de maximitzar la probabilitat de encertar un número correcte.
	def resol(self):
		# ** CEL·LES AMB NOMÉS UNA OPCIÓ POSSIBLE **
		for posicio in range(81):
			# Si encara no hi ha cap número associat...
			if self.numero[posicio] == 0:
				# Mirem si només hi ha un número disponible (només un bit a 1)
				disponibles = self.disponibles[posicio]
				if Sudoku.nbits9[disponibles] == 1:
					# Mirem quin número està associat al bit, l'assignem, i resolem el subproblema
					numero = Sudoku.altbits9[disponibles]
					self.assigna_numero(posicio, numero)
					return self.resol()

		# ** PAS DE PODA **
		simplificats = self.busca_eliminacions()
		if simplificats > 0:
			return self.resol()

		# ** PROVA I ERROR **

		# Busquem la cel·la que tingui menys números disponibles,
		# per tal de maximitzar la probabilitat de fer una prova encertada
		celaMenysDisponibles = None
		nMenysDisponibles = float("inf")

		for posicio in range(81):
			# Si encara no hi ha cap número associat...
			if self.numero[posicio] == 0:
				# Mirem si es el que te menys números disponibles
				nDisponibles = Sudoku.nbits9[self.disponibles[posicio]]
				if nDisponibles < nMenysDisponibles:
					celaMenysDisponibles = posicio
					nMenysDisponibles = nDisponibles

		if celaMenysDisponibles == None: # No queden més cel·les per omplir -> tenim una solució
			return True

		# Fem una còpia del Sudoku, per tal de recuperar l'estat si la prova fracassa
		copia_numero = self.numero[:]
		copia_disponibles = self.disponibles[:]
		self.nProves += 1

		# Anem provant cada número dels possibles
		# Anem mirant quins números ens queden per provar (mirant els bits a 1) i provant-los
		bitsProva = self.disponibles[celaMenysDisponibles]
		while bitsProva != 0: # Mentre queda algun número per provar
			# Mirem a quin bit correspon, l'assignem i provem de resoldre el subproblema
			numero = Sudoku.altbits9[bitsProva]
			self.assigna_numero(celaMenysDisponibles, numero)
			if self.resol():
				return True

			# No hem encertat -> Recuperem l'estat anterior
			self.numero = copia_numero[:]
			self.disponibles = copia_disponibles[:]

			# Ja hem vist que aquest número no serveix, el treiem de la llista de disponibles
			bitsProva &= ~(2**(numero-1))

		return False # Cap prova ha trobat una solució -> no té solució

	# Procés de poda de les opcions disponibles per cada cel·la.
	#
	# Molt sovint, es poden descartar possibilitats en un Sudoku observant
	# quines opcions estan disponibles en altres cel·les. Per exemple,
	# en una regió que conté 3 cel·les buides amb les següents possiblitats:
	# Cel·la 1: [3, 6]                    Cel·la 1: [3, 6]
	# Cel·la 2: [3, 6]                 => Cel·la 2: [3, 6]
	# Cel·la 3: [3, 6, 7]                 Cel·la 3: [7]
	#
	# Es poden donar combinacions més complexes, per exemple:
	# Cel·la 1: [1, 2]                    Cel·la 1: [1, 2]
	# Cel·la 2: [1, 2]                    Cel·la 2: [1, 2]
	# Cel·la 3: [1, 2, 3, 4]           => Cel·la 3: [3, 4]
	# Cel·la 4: [1, 2, 3, 4]              Cel·la 4: [3, 4]
	#
	# En general, si trobem un subconjunt de K cel·les que conté K possibilitats,
	# aleshores podem eliminar aquestes K possibilitats de la resta de cel·les.
	#
	# Fem aquesta cerca per força bruta, mirant tot el conjunt potència.
	# Aixó pot semblar molt lent, però un backtracking encara ho és més.
	#
	# Rep com a paràmetre una regió (fila, columna, quadrat) del Sudoku on aplicar la poda.
	# Retorna el nombre de possibilitats descartades per procés de poda.
	def busca_eliminacions(self):
		simplificats = 0

		for regio in Sudoku.regions:
			# Creem una llista de cel·les buides, que és on intentarem fer les eliminacions
			celes_buides = [posicio for posicio in regio if self.numero[posicio] == 0]

			for r in range(2, len(celes_buides)):
				# Iterem sobre tots els subconjunts diferents de tamany r
				import itertools
				for comb in itertools.combinations(celes_buides, r):
					# Fem la unió dels conjunts de números possibles
					disponibles_total = 0
					for c in comb:
						disponibles_total |= self.disponibles[c]

					# Si tenim K números possibles en K cel·les, els eliminem de la resta de cel·les
					if Sudoku.nbits9[disponibles_total] == len(comb):
						for nc in set(celes_buides) - set(comb):
							simplificats += Sudoku.nbits9[self.disponibles[nc] & disponibles_total]
							self.disponibles[nc] &= ~disponibles_total

		return simplificats

	# Crea una representació textual del Sudoku
	def __str__(self):
		return "\n".join(["".join([str(self.numero[f*9+c]) for c in range(9)]) for f in range(9)])

# Anàlisi de l'eficiència
# -----------------------
# No és plausible donar una complexitat concreta de l'algorisme en termes de la notació O gran,
# ja que depén en gran mesura de detalls de cada taulell de Sudoku i de detalls d'implementació.
#
# # A la pràctica, la majoria de Sudokus es resolen en poques centèsimes de segon,
# amb alguns casos extrems que poden arribar a tardar uns pocs segons.
# (Proves realitzades en un ordinador de la facultat).
# 
# S'han implementat les següents optimizacions per millorar l'eficiència de l'algorisme:
# - Abans d'aplicar cap procés, es busca si hi ha caselles a les quals només hi pugui anar un
#   únic número. Si se'n troba alguna, aleshores es posa aquell número i es continua la resolució.
# - Quan s'ha de fer una prova i error, s'elegeix sempre la casella que tingui el mínim nombre de
#   numeros plausibles. Així es millora la probabilitat de que la prova i error encerti el número.
# - A més a més, s'ha implementat un pas de poda (explicat a busca_eliminacions) que generalment
#   millora l'eficiència de l'algorisme, reduïnt el nombre de proves i errors a fer.
#
# Proves de velocitat (llista de 1465 Sudokus):
# Temps (amb poda): 36 segons
# Temps (sense poda): 264 segons

# Converteix una cadena de text a una matriu de Sudoku
def text_a_matriu_sudoku(text):
	# Filtrem el text segons els caràcters que considerem vàlids per un Sudoku
	# Nosaltres considerem vàlids els dígits de l'1 al 9, i el punt i el 0 com a caselles buides
	caracters_valids = [c for c in text if c in ".0123456789"]
	if len(caracters_valids) < 81: # No hi ha prou informació
		return None

	# Passem els continguts a la matriu
	matriu_sudoku =[[0 for c in range(9)] for f in range(9)]
	for pos,c in enumerate(caracters_valids[:81]):
		if c in "123456789":
			matriu_sudoku[pos/9][pos%9] = int(c)
	return matriu_sudoku

# Llegeix un fitxer que conté un Sudoku
def llegir_sudoku_matriu(fitxer):
	return text_a_matriu_sudoku(open(fitxer).read())

# Comprova si un Sudoku (donada per una matriu) és resoluble
def satisfact(S):
	# Passem els continguts de la matriu del Sudoku a una instància de Sudoku
	s = Sudoku()
	for n in range(81):
		if S[n/9][n%9] != 0:
			if not s.assigna_numero(n, S[n/9][n%9]):
				print "El taulell introduït té incompatibilitats trivials"
				return

	# Intentem resoldre el Sudoku
	import time
	t0 = time.time()
	resoluble = s.resol()
	tf = time.time()

	# Imprimim el resultat
	print "Resoluble:",resoluble
	if resoluble:
		print s
	print "Temps:",tf-t0
	print "Nº de proves:",s.nProves

# Mètode principal del programa. Demana un fitxer de Sudoku i el resol.
def sudoku():
	nom_fitxer = raw_input("Introdueix el nom del fitxer: ")
	matriu_sudoku = llegir_sudoku_matriu(nom_fitxer)
	if matriu_sudoku != None:
		satisfact(matriu_sudoku)
	else:
		print "No es pot interpretar aquest fitxer com un Sudoku"

# Realitza una prova de resolució de Sudokus (llista de 1465 Sudokus difícils)
def prova_velocitat():
	import urllib,time
	llista_sudokus = urllib.urlopen("http://magictour.free.fr/top1465").read()

	t0 = time.time()
	for sudoku in llista_sudokus.split():
		matriu_sudoku = text_a_matriu_sudoku(sudoku)
		satisfact(matriu_sudoku)
	tf = time.time()
	print "Temps total:",tf-t0

sudoku()
#prova_velocitat()
